package com.example.assignment;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class GetNearbyPlacesData extends AsyncTask<Object, String, String> {
    GoogleMap map;
    String url;
    InputStream in;
    BufferedReader bufferedReader;
    StringBuilder stringBuilder;
    String data;
    String TAG = "Nearbyplaces";
    CommonInterface commonInterface =null;

    @Override
    protected String doInBackground(Object... params) {

        url = (String) params[0];
        try {
            URL myurl = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) myurl.openConnection();
            httpURLConnection.connect();
            in = httpURLConnection.getInputStream();
            bufferedReader = new BufferedReader(new InputStreamReader(in));
            stringBuilder = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
            data = stringBuilder.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }
/*
* Callback in activity through common interface listerner
* */
    @Override
    protected void onPostExecute(String s) {
        Log.d(TAG, "onPostExecute: "+s+"");

        commonInterface.getResult(s);


        super.onPostExecute(s);
    }
}